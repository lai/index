
# Don't expand empty directories
shopt -s nullglob;

create-manifest()
{
    echo "<div>"
           
    # Add option to go back up the tree
    echo "<a id="TitleLink" href=../>"
    if [ -f "meta.json" ]; then
        jq '.title' < "meta.json" | tr -d '"'
    else
        echo "$1"
    fi

    echo "</a><br><br>"
    for folder in */; do 
        echo "<a href='$folder'>"
        
        # In the folder, if there's a meta.json file, use jq to extract attributes
        if [ -f "$folder/meta.json" ]; then 
            jq '.title' < "$folder/meta.json" | tr -d '"'
        else
            echo "$folder"
        fi
        echo "</a><br>"
       
        (
           cd $folder;
           echo "<ul>"
           for subfolder in */; do
               echo "<li><a href='$folder/$subfolder'>" 
                # In the folder, if there's a meta.json file, use jq to extract attributes
                if [ -f "$subfolder/meta.json" ]; then 
                    jq '.title' < "$subfolder/meta.json" | tr -d '"'
                else
                    echo "$subfolder"
                fi
                echo "</a></li>"
           done
           echo "</ul>"
        ) 
         done
    echo "</div>"
}

# Recursively generate manifest for each subdirectory
traverse()
{
    for folder in */; do
        (
            echo $folder;
            cd $folder;
            create-manifest $folder > dirs.html
            traverse            
        )
    done
}


