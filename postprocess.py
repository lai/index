from bs4 import BeautifulSoup
import sys

def main():
    with open("template.txt") as fp:
        template = fp.read()
    with open(sys.argv[1]) as fp:
        dirs = fp.read()
    soup = BeautifulSoup(sys.stdin.read(), "html.parser")
    print(template.replace("%CONTENT%", soup.prettify()).replace("%TREE%", dirs))
if __name__ == "__main__":
    main()
