source create_manifest.sh

traverse # Create dirs

for f in $(find . -maxdepth 10 -name "*.wiki"); do
   HTML=${f%.*}.html
   WIKI=${f%.*}.wiki
   echo "Building $WIKI" 
   manifest=$(dirname  $f)/dirs.html
   cat "$WIKI" | python3 preprocess.py $(dirname $f) | pandoc -t html -f vimwiki | python3 postprocess.py "$manifest" > "$HTML";

done


