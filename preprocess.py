# This module executes inline code
import sys
import os
from html.parser import HTMLParser
import collections
from urllib.parse import urlparse
from subprocess import Popen, PIPE

class Job():

    def __init__(self, attrs, id):
        self.attrs = attrs
        self.id = id
        self.code = None

    def execute(self):
        assert self.code
        filename = "tmpjob{self.id}"
        with open(filename, "w") as fp:
            fp.write(self.code)
        try:
            
            prog = ""
            if self.attrs["lang"] == "python3":
                prog = "python3"
            elif self.attrs["lang"] == "dot":
                prog = "dot"

            child = Popen([prog, filename], stdin=PIPE, stdout=PIPE)
            output, err = child.communicate()
            if err:
                print("ERROR", file=sys.stderr)
            # Replace the code block with the output of the code section
            if self.attrs["render"] == "stdout":
                print(output.decode("utf-8"))
            elif self.attrs["render"]:
                uri = urlparse(self.attrs["render"])
                if uri.scheme == "image":
                    print("{{" + uri.path +"}}")
            
            if self.attrs["stdout"]:
                with open(self.attrs["stdout"], "w") as fp:
                    fp.write(output.decode("utf-8"))


        finally:
            os.remove(filename)


class InlineParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self.in_context = False
        self.jobs:Job = []

    def handle_data(self, data):
        if not self.in_context:
            print(data)
        else:
            assert self.jobs 
            self.jobs[-1].code = data
            self.jobs[-1].execute() 

    def handle_starttag(self, tag, attrs):
        if tag == "inline":
            assert not self.in_context
            self.in_context = True
            attrdict = collections.defaultdict(lambda:None, attrs)
            self.jobs.append(Job(attrdict,0))

    def handle_endtag(self, tag):
        if tag == "inline":
            assert self.in_context
            self.in_context = False


def main():
    os.chdir(sys.argv[1])    
    data = sys.stdin.read()
    parser = InlineParser()
    parser.feed(data)



if __name__ == "__main__":
    main()
