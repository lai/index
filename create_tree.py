import os


def create_tree(path, root):
    
    join = lambda f: os.path.join(path, f)

    folders = [ f for f in os.listdir(path) if os.path.isdir(join(f))]
    files = [ f for f in os.listdir(path) if os.path.isfile(join(f)) and f.endswith(".wiki")]
    
    relpath = path.replace(root, "")
    for f in folders:
        print(f"<a src={relpath}></a>")
        create_tree(join(f), root)
def main():
    
    create_tree(os.getcwd() + "/src", os.getcwd() + "/src")
if __name__ == "__main__":
    main()
