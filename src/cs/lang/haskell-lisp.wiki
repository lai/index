= Haskell && Lisp =

Some list operations

| Haskell | Lisp | Descr                             |
|---------|------|-----------------------------------|
| head    | car  | First element of list             |
| tail    | cdr  | All elements except first element |
| last    | last | Last element of list              |
|         |      |                                   |
